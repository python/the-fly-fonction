#!/usr/bin/python
# -*- coding: utf-8 -*-

from pylab import *

def pgcd(a,b):
    if b==0: return a
    else:
        r=a%b
        return pgcd(b,r)

def fly(n,a_n) :
    if n==0 or n==1 : return 1
    elif pgcd(n,a_n)==1 : return a_n+n+1
    else : return a_n//pgcd(n,a_n)

y=1
for n in range(0,1000) :
    y=fly(n,y)
    scatter(n,y)

show()